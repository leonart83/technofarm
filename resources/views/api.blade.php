@extends('layouts.app')
@section('head_css')

@endsection
@section('content')

    {{    dump(\Route::getRoutes() )  }}

    <div class="container">
        <div class="row">
            <ul class="list-group">
                <li class="list-group-item justify-content-between">
                    Cras justo odio
                    <span class="badge badge-default badge-pill">14</span>
                </li>
                <li class="list-group-item justify-content-between">
                    Dapibus ac facilisis in
                    <span class="badge badge-default badge-pill">2</span>
                </li>
                <li class="list-group-item justify-content-between">
                    Morbi leo risus
                    <span class="badge badge-default badge-pill">1</span>
                </li>
            </ul>
        </div>
    </div>
@endsection
@push('post-scripts')
<!-- bootstrap datepicker -->
<script src="{{asset('public/plugins/vue/vue.2.2.0.min.js')}}"></script>

@endpush
