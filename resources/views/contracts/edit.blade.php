@extends('layouts.app')
@section('head_css')
    <style>
        .nav-collapse {
            max-height: 1200px;
            overflow-y: auto;
        }
        .fade-enter-active, .fade-leave-active {
            transition: opacity .5s
        }
        .fade-enter, .fade-leave-to /* .fade-leave-active in <2.1.8 */
        {
            opacity: 0
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="container">
            <div id="main" class="row">
                <div class="col-md-6 ">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div v-show="interactive" class="pull-right">
                                <i class="fa fa-spin fa-refresh"></i>
                            </div>
                            <h3>Contract Dashboard</h3>
                            <h4 class="box-title">Edit Contract and create Plots</h4>
                            <transition name="fade"></transition>
                        </div>
                        <div class="panel-body">
                            <div class="box box-primary">
                                <div class="box-header with-border"></div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                {!! Form::open( ['route' => ['api.v1.contracts.update',$id],'method' => 'PUT','id' => 'contract_form']) !!}
                                {!! method_field('PUT') !!}
                                {!! Form::hidden('id',$id) !!}
                                <input name="_method" type="hidden" value="PUT">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Select Contract Type</label>
                                        <select v-on:change="resetPrice" name="contract_type"
                                                v-model="contract.contract_type" class="form-control">
                                            <option>Lease</option>
                                            <option>Ownership</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Start Date</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input name="start_date" v-model="contract.start_date" type="date"
                                                   class="form-control"
                                                   data-inputmask="'alias': 'dd/mm/yyyy'"
                                                   data-mask="">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                        <label>Due Date</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input name="due_date" v-model="contract.due_date" type="date"
                                                   class="form-control"
                                                   data-inputmask="'alias': 'dd/mm/yyyy'"
                                                   data-mask="">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    <div class="form-group">
                                        <label>Rent Price</label>
                                        <input name="rent_hectare" v-model="contract.rent_hectare" type="number"
                                               class="form-control" v-bind:readonly="pricesReadOnly"
                                               step="any" min="0" value=""
                                               placeholder="Enter Rent price per hectare">
                                    </div>
                                    <div class="form-group">
                                        <label>Price</label>
                                        <input name="price" v-model="contract.price" type="number"
                                               class="form-control" v-bind:readonly="!pricesReadOnly"
                                               step="any" min="0" value=""
                                               placeholder="Enter Price...">
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button id="submit_btn" type="submit" class="btn btn-primary">Submit</button>
                                    <button v-on:click="showPlotForm" type="button" class="btn btn-success">Show Create
                                        Plot
                                        Form
                                    </button>
                                </div>
                                {{ Form::close() }}
                                <br><div v-show="haveMessage" class="alert" v-bind:class="alertStyle">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                    </button>
                                    <h4 v-show="success !== null"><i class="icon fa fa-check"></i> Success!</h4>
                                    <h4 v-show="errors  !== null"><i class="icon fa fa-close"></i> Error!</h4>
                                    @{{success}}
                                    @{{errors}}
                                </div><br>
                            </div>
                            <div id="myModal" class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Confirmation</h4>
                                        </div>
                                        <div class="modal-body">
                                            <label>Select Owner for plot @{{ selectedPlot }}</label>
                                            <select id="selected_owner" name="selected_owner" class="form-control">
                                                <option v-for="owner in owners"
                                                        :value="owner.id">@{{owner.name + owner.surname}}</option>

                                            </select>
                                            <p class="text-warning">
                                                <small></small>
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button v-on:click="saveOwnerToPlot" id="save_owner" type="button"
                                                    class="btn btn-primary">Save changes
                                            </button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
                                            </button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> {{--col-md-offset-1--}}
                <div class="col-md-6 ">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div v-show="interactive" class="pull-right">
                                <i class="fa fa-spin fa-refresh"></i>
                            </div>
                            <h3>Plots Dashboard</h3>
                            <h4 class="box-title">Assign Plots to Land Owners</h4>
                            <transition name="fade">

                            </transition>
                        </div>
                        <div class="panel-body">
                            <div class="box box-primary">
                                <div class="box-header with-border"></div>
                                <div class="box-body">
                                    <br>
                                    <transition name="fade">
                                        <div id="plot_form_group" v-show="show_plot">@include('contracts.plots')</div>
                                    </transition>
                                    <br>
                                    <div class="nav-collapse">
                                        <ul class="nav nav-stacked" v-for="plot in contract.plots">
                                            <li>Plot id :@{{plot.id}} <a href="#">
                                                    Plot Unique Id : @{{plot.unique_id}}<span
                                                            class="pull-right badge bg-green">Area : @{{plot.area}} </span></a>
                                            <li class="nav nav-stacked" v-for="owner in plot.owners">
                                                owner : @{{owner}}
                                            </li>
                                            <li class="nav nav-stacked" v-if="contract.contract_type == 'Lease'">
                                                <button v-on:click="showModal(plot)" type="button"
                                                        class="btn btn-info btn-sm pull-right">Add Owner
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="box-footer">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('post-scripts')
<script src="{{asset('public/plugins/vue/vue.2.2.0.min.js')}}"></script>
<script type="text/javascript">
    /** <!-- Model --> */
    function Plot() {
        this.area = $("#plot_area").val();
        this.unique_id = 'generated after saving, please wait';
        return this;
    }
    /** <!-- End Model --> */
    /** <!-- Vue Logic --> */
    var V = new Vue({
        el: '#main',
        debug: true,
        data: {
            contract: [],
            owners: null,
            errors: null,
            success: null,
            selectedPlot: null,
            show_plot: false,
            interactive: null,
        },
        mounted: function () {
            var self = this;
            $.ajax({
                url: '{{url( 'api/v1/contracts/'.$id )}}',
                method: 'get',
                dataType: 'json',
                success: function (response) {
                    self.contract = response.data[0];
                    if (response.status) {
                        self.success = response.message;
                    }
                    self.errors = null;
                },
                error: function (response) {
                    if (response.responseJSON.errors) {
                        self.errors = response.responseJSON.errors;
                    }
                    self.success = null;
                }
            });
            $.ajax({
                url: '{{url( 'api/v1/owners/' )}}',
                method: 'get',
                dataType: 'json',
                success: function (response) {
                    //console.log(response);
                    self.owners = response.data;
                    //console.log(self.owners);
                    if (response.status) {
                        self.success = response.message;
                    }
                    self.errors = null;
                },
                error: function (response) {
                    if (response.responseJSON.errors) {
                        self.errors = response.responseJSON.errors;
                    }
                    self.success = null;
                }
            });
            $('#contract_form').submit(function (event) {
                event.preventDefault();
                var $form = $(this);
                $.ajax({
                    type: 'PUT',
                    url: $form.attr('action'),
                    data: $form.serializeArray(),
                    success: function (response) {
                        console.log(response);
                        self.contract = response.data;
                        if (response.status) {
                            self.success = response;
                        }
                        self.errors = null;
                    },
                    error: function (response) {
                        console.log(response);
                        if (response.responseJSON.errors) {
                            self.errors = response.responseJSON.errors;
                        }
                        self.success = null;
                    }
                });
            });
            $('#plot_form').submit(function (event) {
                event.preventDefault();
                var $form = $(this);
                $.ajax({
                    type: $form.attr('method'),
                    url: $form.attr('action'),
                    data: $form.serializeArray(),
                    success: function (response) {
                        console.log(response);
                        self.contract = response.data;
                        if (response.status) {
                            self.success = response;
                        }
                        self.errors = null;
                    },
                    error: function (response) {
                        console.log(response);
                        self.contract.plots.splice(-1, 1);
                        if (response.responseJSON.errors) {
                            self.errors = response.responseJSON.errors;
                        }
                        self.success = null;
                    }
                });
            });
            $(document).bind("ajaxSend", function () {
                self.interactive = true;
                //$('<div class="modal-backdrop"></div>').appendTo(document.body);
            }).bind("ajaxComplete", function () {
                self.interactive = false;
                //$(".modal-backdrop").remove();
            });
        },
        methods: {
            resetPrice: function () {
                console.log(this.contract.contract_type);
                if (this.contract.contract_type == 'Lease') {
                    this.contract.price = null;

                } else if (this.contract.contract_type == 'Ownership') {
                    this.contract.rent_hectare = null;

                } else {
                    throw new Error('Wrong contracty type');
                }
            },
            savePlot: function (event) {
                var plot = new Plot();
                this.contract.plots.push(plot);
            },
            showModal: function (item) {
                this.selectedPlot = item;
                $("#myModal").modal('show');
            },
            showPlotForm: function () {
                this.show_plot = !this.show_plot;
            },
            saveOwnerToPlot: function () {
                var self = this;
                var data = {};
                data.plot_id = this.selectedPlot.id;
                data.owner_id = $('#selected_owner').val();
                $.ajax({
                    type: 'PUT',
                    url: "{{ url('api/v1/plots/')}}" + '/' + data.plot_id,
                    data: data,
                    success: function (response) {
                        console.log(response);
                        for (var j = 0; j < self._data.contract.plots.length; j++) {
                            console.log(self._data.contract.plots[j].unique_id);
                            if (self._data.contract.plots[j].unique_id == response.data.unique_id) {
                                self._data.contract.plots[j] = response.data;
                                $("#myModal").modal('hide');
                            }
                        }
                        if (response.status) {
                            if (response.status) {
                                self.success = response;
                            }
                            self.errors = null;
                        }
                    },
                    error: function (response) {
                        console.log(response);
                        if (response.responseJSON.errors) {
                            self.errors = response.responseJSON.errors;
                        }
                        self.success = null;
                    }
                });
            }
        },
        computed: {
            haveMessage: function () {
                return !(this.success == null && this.errors == null);
            },
            alertStyle: function () {
                return {
                    'alert-success': (this.success && !this.error),
                    'alert-danger': !(this.success && !this.error)
                }
            },
            pricesReadOnly: function (event) {
                return this.contract.contract_type == 'Lease' ? false : true;
            },
        }
    });

    Vue.config.devtools = true;
    /** <!-- End Vue Logic --> */

    $('.alert').on('close.bs.alert', function (e) {
        e.preventDefault();
        V._self._data.success = null;
        V._self._data.errors = null;
    });
</script>
@endpush

