{!! Form::open( ['route' => ['api.v1.plots.store'],'method' => 'POST','id' => 'plot_form']) !!}
<div class="box-body">
    <input name="contract_id" type="hidden" v-model="contract.id">
    <div class="form-group">
        <label>area</label>
        <input id="plot_area" name="area" type="number" class="form-control" step="any" min="0" value=""
               placeholder="Enter Area...">

    </div>
</div>

<!-- /.box-body -->
<div class="box-footer">
    <button v-on:click="savePlot"  type="submit" class="btn btn-primary">Save Plot</button>
</div>
{{ Form::close() }}