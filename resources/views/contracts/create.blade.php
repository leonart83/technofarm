@extends('layouts.app')
@section('head_css')
    <!-- bootstrap datepicker -->
{{--
    <link rel="stylesheet" href="{{asset('public/plugins/datepicker/datepicker3.css')}}">
--}}
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Contract Create</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            {{ Form::open( ['route' => 'api.v1.contracts.store','method' => 'post','id' => 'create_form']) }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Select Contract Type</label>
                                    <select name="contract_type" class="form-control">
                                        <option>Lease</option>
                                        <option>Ownership</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Start Date</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="start_date" type="date" class="form-control"
                                               data-inputmask="'alias': 'dd/mm/yyyy'"
                                               data-mask="" value="1983-06-22">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group">
                                    <label>Due Date</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input name="due_date" type="date" class="form-control"
                                               data-inputmask="'alias': 'dd/mm/yyyy'"
                                               data-mask="" value="1983-06-22">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group">
                                    <label>Rent Price</label>
                                    <input name="rent_hectare" type="number" class="form-control" step="any" min="0"
                                           value=""
                                           placeholder="Enter Rent price per hectare">

                                </div>
                                <div class="form-group">
                                    <label>Price</label>
                                    <input name="price" type="number" class="form-control" step="any" min="0" value=""
                                           placeholder="Enter Price...">

                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button id="submit_btn" type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            <div class="box-footer">
                                <div v-if="success" class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                    </button>
                                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                                    @{{success}}
                                </div>
                                <div v-if="errors" class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×
                                    </button>
                                    <h4><i class="icon fa fa-check"></i> Error!</h4>
                                    @{{errors}}
                                </div>
                            </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('post-scripts')

<script src="{{asset('public/plugins/vue/vue.2.2.0.min.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{asset('public/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- Page script -->
<script>

    new Vue({
        el: '#create_form',
        debug: true,
        data: {
            errors: null,
            success: null
        },
        mounted: function () {
            var self = this;
            console.log('ready');
            $('#create_form').submit(function (event) {
                event.preventDefault();
                var $form = $(this);

                $.ajax({
                    type: $form.attr('method'),
                    url: $form.attr('action'),
                    data: $form.serializeArray(),
                    success: function (response) {
                        console.log(response);

                        if (response.status) {
                            self.success = response;
                            self.errors = null;
                        }
                    },
                    error: function (response) {
                        console.log(response);
                        self.errors = response.responseJSON.errors;
                        self.success = null;
                    }
                });

            });
        }
    });

    Vue.config.devtools = true;
</script>
@endpush

