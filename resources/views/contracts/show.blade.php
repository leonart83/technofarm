@extends('layouts.app')
@section('head_css')
    <!-- bootstrap datepicker -->
    {{--
        <link rel="stylesheet" href="{{asset('public/plugins/datepicker/datepicker3.css')}}">
    --}}
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Contract Dashboard</div>

                    <div id="main" class="panel-body">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">All Contracts</h3>
                            </div>
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Data Table With Full Features</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                        <div class="row">
                                          {{--  <div class="col-sm-6">
                                                <div class="dataTables_length" id="example1_length"><label>Show <select
                                                                name="example1_length" aria-controls="example1"
                                                                class="form-control input-sm">
                                                            <option value="10">10</option>
                                                            <option value="25">25</option>
                                                            <option value="50">50</option>
                                                            <option value="100">100</option>
                                                        </select> entries</label></div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div id="example1_filter" class="dataTables_filter"><label>Search:<input
                                                                type="search" class="form-control input-sm"
                                                                placeholder="" aria-controls="example1"></label></div>
                                            </div>--}}
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table id="example1"
                                                       class="table table-bordered table-striped dataTable" role="grid"
                                                       aria-describedby="example1_info">
                                                    <thead>
                                                    <tr role="row">
                                                        <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                            rowspan="1" colspan="1" aria-sort="ascending"
                                                            aria-label="Rendering engine: activate to sort column descending"
                                                            style="width: 259.182px;">Contract Number
                                                        </th>
                                                        <th class="sorting" tabindex="0" aria-controls="example1"
                                                            rowspan="1" colspan="1"
                                                            aria-label="Browser: activate to sort column ascending"
                                                            style="width: 319.182px;">Date of ownership
                                                        </th>
                                                        <th class="sorting" tabindex="0" aria-controls="example1"
                                                            rowspan="1" colspan="1"
                                                            aria-label="Platform(s): activate to sort column ascending"
                                                            style="width: 282.182px;">Area
                                                        </th>
                                                        <th class="sorting" tabindex="0" aria-controls="example1"
                                                            rowspan="1" colspan="1"
                                                            aria-label="Engine version: activate to sort column ascending"
                                                            style="width: 225.182px;">Price
                                                        </th>

                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr role="row" class="even">
                                               {{--     {{dd($data)}}--}}
                                                    @foreach($data as $index=>$row)

                                                            <tr>
                                                                <td>{{$row['id'] }} </td>
                                                                <td>{{$row['start_date'] }} </td>
                                                                <td>{{$row['area'] }} </td>
                                                                <td>{{$row['price_total'] }} </td>

                                                            </tr>

                                                    @endforeach


                                                     {{--   <td class="sorting_1">Gecko</td>
                                                        <td>Mozilla 1.0</td>
                                                        <td>Win 95+ / OSX.1+</td>
                                                        <td>1</td>
                                                    </tr>--}}
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th rowspan="1" colspan="1">Contract Number</th>
                                                        <th rowspan="1" colspan="1">Date of ownership</th>
                                                        <th rowspan="1" colspan="1">Area</th>
                                                        <th rowspan="1" colspan="1">Price</th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-5">

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('post-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>


<!-- page script -->
<script src="{{asset('public/plugins/vue/vue.2.2.0.min.js')}}"></script>
<!-- Page script -->
<script>
    $(function () {

        $('#example1').DataTable({

        });
    });
</script>
@endpush

