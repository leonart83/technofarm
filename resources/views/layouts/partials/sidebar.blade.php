@extends('layouts.admin')
@section('sidebat')
<div class="wrapper">
    <header class="main-header">
        <nav class="navbar navbar-static-top" role="navigation">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">превключване навигация</span>
            </a>
        </nav>
    </header>
    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu">
                <li class="header">НАВИГАЦИЯ</li>
                <li class="{{ (Request::is('admin'))? 'active': ''  }}"><a href="{!! url('admin/') !!}">
                        <i class="fa fa-link">
                        </i><span>Начало</span></a></li>
                <li class="{{ (Request::is('admin/blog*'))? 'active': ''  }}"><a href="{!! url('admin/blog/list') !!}">
                        <i class="fa fa-comment"></i>
                        <span>Блог Статии</span></a></li>
                <li class="{{ (Request::is('admin/contacts*'))? 'active': ''  }}"><a href="{!! url('admin/contacts/') !!}">
                        <i class="fa fa-pencil"></i>
                        <span>Контакти</span></a></li>
                <li class="{{ (Request::is('admin/messages*'))? 'active': ''  }}"><a href="{!! url('admin/messages/') !!}">
                        <i class="fa fa-edit"></i>
                        <span>Съобщения</span></a></li>
                <li class="{{ (Request::is('admin/gallery*'))? 'active': ''  }}"><a href="{!! url('admin/gallery') !!}">
                        <i class="fa fa-star"></i>
                        <span>Галерия</span></a>
                <li class="{{ (Request::is('admin/categories*'))? 'active': ''  }}"><a href="{!! url('admin/categories') !!}">
                        <i class="fa fa-folder"></i>
                        <span>Категории</span></a>
                <li class="{{ (Request::is('admin/subcategories*'))? 'active': ''  }}"><a href="{!! url('admin/subcategories') !!}">
                        <i class="fa fa-folder-open"></i>
                        <span>Подкатегории</span></a>
                <li class=""><a href="{!! url('logout') !!}">
                        <i class="fa fa-sign-out"></i>
                        <span>Изход</span></a>
                </li>
            </ul>
        </section>
    </aside>
    @yield('content')

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            Showtime Administration Panel
        </div>
        <strong>Copyright &copy; 2016 <a href="#">Showtime</a>.</strong> All rights reserved.
    </footer>
</div>

@endsection