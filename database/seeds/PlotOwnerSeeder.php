<?php

use Illuminate\Database\Seeder;

class PlotOwnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\PlotOwner::truncate();
        $contracts_id = DB::table('contracts')->where('contract_type', '=', 'Lease')->pluck('id');
        $plots_id = DB::table('plots')->whereIn('id', $contracts_id)
            ->get();
        $owners = DB::table('owners')->take(5)->get();
        $array = []    ;
        foreach ($plots_id as $plot){
            $temp = [];
            $temp['plot_id'] = $plot->id;
            $temp['owner_id'] = $owners[rand(0,4)]->id;

            $array[] = $temp;
        }
        DB::table('plot_owner')->insert( $array);
    }
}
