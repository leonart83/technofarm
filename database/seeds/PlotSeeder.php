<?php

use Illuminate\Database\Seeder;

class PlotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Plot::truncate();
        factory(App\Plot::class, 20)->create(
        )->each(function ($i) {
            $model = factory(App\Plot::class)->make()->toArray();
            $i->save($model);
        });
    }
}
