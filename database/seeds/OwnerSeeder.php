<?php

use Illuminate\Database\Seeder;

class OwnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Owner::truncate();
        factory(App\Owner::class, 8)->create()->each(function ($i) {
            $i->save(factory(App\Owner::class)->make()->toArray());
        });
    }
}
