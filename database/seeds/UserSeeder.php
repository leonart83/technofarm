<?php

use Illuminate\Database\Seeder;

/**
 * Class UserSeeder
 *
 * artisan db:seed --class=UserSeeder
 */
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::truncate();
        factory(App\User::class, 'Farmer', 1)->create()->each(function ($i) {
            $i->save(factory(App\User::class)->make()->toArray());
        });
    }
}
