<?php

use Illuminate\Database\Seeder;

class ContractSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Contract::truncate();
        /** I used different factory model here, a default one, and 2 more specific, depending on the contract.
         * The result its the same */
        factory(App\Contract::class, 4)->create()->each(function ($i) {
            $i->save(factory(App\Contract::class)->make()->toArray()
            );
        });
        factory(App\Contract::class, 'Lease', 4)->create()->each(function ($i) {
            $i->save(factory(App\Contract::class)->make()->toArray()
            );
        });
        factory(App\Contract::class, 'Ownership', 4)->create()->each(function ($i) {
            $i->save(factory(App\Contract::class)->make()->toArray()
            );
        });
    }
}
