<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * @var string
     */
    private $table = 'contracts';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      //  if (!Schema::hasTable($this->table)) {
            Schema::create($this->table,
                function (Blueprint $table) {
                    $table->increments('id')->unsigned();
                    $table->enum('contract_type', App\Contract::$contract_types);
                    $table->date('start_date')->nullable();
                    $table->date('due_date')->nullable();
                    $table->unsignedInteger('rent_hectare')->nullable();
                    $table->unsignedInteger('price')->nullable();

                }
            );
     //   }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop($this->table);
    }
}
