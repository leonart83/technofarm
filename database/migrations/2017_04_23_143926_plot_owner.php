<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlotOwner extends Migration
{
    /**
     * @var string
     */
    private $table = 'plot_owner';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table,
                function (Blueprint $table) {
                    $table->increments('id')->unsigned();
                    $table->unsignedInteger('plot_id');
                    $table->foreign('plot_id')->references('id')->on('plots');

                    $table->unsignedInteger('owner_id');
                    $table->foreign('owner_id')->references('id')->on('owners');



                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table, function (Blueprint $table) {
            if (Schema::hasColumn($this->table, 'plot_id')) {
                $table->dropForeign(['plot_id']);
            }
            if (Schema::hasColumn($this->table, 'owner_id')) {
                $table->dropForeign(['owner_id']);
            }
        });
        Schema::drop($this->table);
    }
}
