<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlots extends Migration
{
    /**
     * @var string
     */
    private $table = 'plots';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table,
                function (Blueprint $table) {
                    $table->increments('id')->unsigned();
                    $table->uuid('unique_id');
                    $table->unsignedInteger('area');
                    $table->unsignedInteger('contract_id');
                    $table->foreign('contract_id')->references('id')->on('contracts');

                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table, function (Blueprint $table) {
            if (Schema::hasColumn($this->table, 'contract_id')) {
                $table->dropForeign(['contract_id']);
            }
        });
        Schema::drop($this->table);
    }
}
