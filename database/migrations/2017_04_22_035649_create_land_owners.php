<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandOwners extends Migration
{
    /**
     * @var string
     */
    private $table = 'owners';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       // if (!Schema::hasTable($this->table)) {
            Schema::create($this->table,
                function (Blueprint $table) {
                    $table->increments('id')->unsigned();
                    $table->string('name', 30);
                    $table->string('surname', 40);
                    $table->string('phone');
                    $table->uuid('personal_id');
                }
            );
       // }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop($this->table);
    }
}
