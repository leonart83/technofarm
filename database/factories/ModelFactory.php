<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name'           => $faker->name,
        'email'          => $faker->safeEmail,
        'password'       => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
        'api_token'      => str_random(60),
    ];
});

$factory->defineAs(App\User::class, 'Farmer', function () {
    return [
        'name'           => 'leo',
        'email'          => 'leo@admin.com',
        'password'       => bcrypt('123456'),
        'remember_token' => str_random(10),
        'api_token'      => str_random(60),
    ];
});
$factory->define(App\Contract::class, function (Faker\Generator $faker) {
    $contract_type = App\Contract::$contract_types[rand(0, count(App\Contract::$contract_types) - 1)];
    return [
        'contract_type' => $contract_type,
        'start_date'    => $faker->dateTime,
        'due_date'      => $faker->dateTime,
        'rent_hectare'  => ($contract_type == 'Lease') ? $faker->numberBetween(100, 900) : null,
        'price'         => ($contract_type == 'Ownership') ? $faker->numberBetween(1000, 1000) : null,
    ];
});
$factory->defineAs(App\Contract::class, 'Lease', function (Faker\Generator $faker) {
    return [
        'contract_type' => 'Lease',
        'start_date'    => $faker->dateTime,
        'due_date'      => $faker->dateTime,
        'rent_hectare'  => $faker->numberBetween(100, 900),
        'price'         => null
    ];
});

$factory->defineAs(App\Contract::class, 'Ownership', function (Faker\Generator $faker) {
    return [
        'contract_type' => 'Ownership',
        'start_date'    => $faker->dateTime,
        'due_date'      => $faker->dateTime,
        'rent_hectare'  => null,
        'price'         => $faker->numberBetween(1000, 1000),
    ];
});


$factory->define(App\Owner::class, function (Faker\Generator $faker) {
    return [
        'name'        => $faker->name,
        'surname'     => $faker->lastName,
        'phone'       => $faker->phoneNumber,
        'personal_id' => $faker->uuid,
    ];
});
/** todo pass App\Contract::count() as argument to query count just once */
$factory->define(App\Plot::class, function (Faker\Generator $faker) {
    return [
        'unique_id'   => $faker->uuid,
        'area'        => $faker->numberBetween(10, 1000),
        'contract_id' => $faker->numberBetween(1, App\Contract::count()),
    ];
});
