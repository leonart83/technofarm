<?php

/**
 * Created by PhpStorm.
 * User: leonardo
 * Date: 24/04/2017
 * Time: 11:21:44
 */
class ApiTest extends \TestCase
{
    /**
     * @var App\User $user
     */

    private $user = null;

    /**
     * @var App\Contract $contract
     */
    private $contract = ['contract_type' => 'Lease','start_Date'];

    protected function setUp()
    {
        parent::setUp();
        $this->user = App\User::first();

        $this->actingAs($this->user);
    }

    /**
     * @return array
     */
    public function ApiProvider()
    {
        return [
            ['GET', '/api/v1/contracts', [], 200],
            ['GET', '/api/v1/contracts/1', [], 200],
            ['POST', '/api/v1/contracts/', $this->contract, 400],
        ];
    }

    /**
     * A basic functional test example.
     *
     * @test
     * @dataProvider ApiProvider
     *
     * @param $method
     * @param $url
     * @param $params
     * @param $expectedResponseCode
     */
    public function testGetPostApi($method, $url, $params, $expectedResponseCode)
    {

        $response = $this->call($method, $url, $params);
        $data = json_decode($response->getContent());
        if ($method == 'POST') dump($data);

        $this->assertEquals($expectedResponseCode, $response->getStatusCode());

        $this->assertObjectHasAttribute('data', $response);
        $this->assertNotNull('data', $data);
        /*  $this->assertObjectHasAttribute('status', $data);*/
        /*   $this->assertObjectHasAttribute('message', $data);*/
        /*   $this->assertObjectHasAttribute('data', $data);*/
        $this->assertSame(JSON_ERROR_NONE, json_last_error());
    }
}
