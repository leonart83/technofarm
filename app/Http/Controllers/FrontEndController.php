<?php

namespace App\Http\Controllers;

use App\Contract;

class FrontEndController extends Controller
{

    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ownReport()
    {
        $data = Contract::with('plots.owners')->where('contract_type', '=', 'Ownership')->get()->toArray();

        return view('contracts.show',compact('data'));
    }

    public function show2()
    {
        return view('contracts.show');
    }
}
