<?php

namespace App\Http\Controllers;

use App\Contract;
use Illuminate\Http\Request;

class ContractsController extends ApiControllerV1
{


    /**
     * Display all the Contracts of the database
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Contract::get();
        return $this->jsonResponse($data);
    }

    /**
     * Show the crate Form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contracts.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input_data = $request->all();
        $response = Contract::validate($input_data);
        if ($response !== true) {
            return $this->jsonErrorResponse($response);
        }
        $data = Contract::store($input_data);

        if (is_a($data, 'Exception')) {
            return $this->jsonExceptionResponse($data);
        }

        return $this->jsonResponse($data, null, 'model created correctly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Contract::whereIn('id', [$id])->get();
        return $this->jsonResponse($data, $id, 'model found and loaded correctly');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('contracts.edit',compact('id'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = Contract::validate($request->all());
        if ($response !== true) {
            return $this->jsonErrorResponse($response);
        }
        $data = Contract::updateModel($request->all());
        if (is_a($data, 'Exception')) {
            return $this->jsonExceptionResponse($data);
        }

        return $this->jsonResponse($data, null, 'model updated correctly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
