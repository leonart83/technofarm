<?php

namespace App\Http\Controllers;

use App\Plot;
use Illuminate\Http\Request;

class PlotController extends ApiControllerV1
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Plot::all();
        return $this->jsonResponse($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = Plot::validate( $request->all() );
        if($response !== true){
            return $this->jsonErrorResponse($response);
        }
        $data =  Plot::store( $request->all() );

        if (is_a($data, 'Exception')) {
            return $this->jsonExceptionResponse($data);
        }

        return $this->jsonResponse( $data , null , 'model created correctly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Plot::find($id);

        return $this->jsonResponse($data , $id  , 'model found and loaded correctly');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       /* $data = Plot::find($id);
        $data->owners()->sync([1, 2, 3]);*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Plot::updateModel( $request->all() );
        if (is_a($data, 'Exception')) {
            return $this->jsonExceptionResponse($data);
        }

        return $this->jsonResponse( $data , $id , 'model updated correctly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
