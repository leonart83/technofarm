<?php
/**
 * Created by PhpStorm.
 * User: leonardo
 * Date: 23/04/2017
 * Time: 08:10:56
 */

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;


abstract class ApiControllerV1 extends Controller
{
    /**
     * @param Collection $data
     *
     * @param null       $id
     * @param null       $msg
     *
     * @return \Illuminate\Http\Response
     */
    protected function jsonResponse($data, $id = null, $msg = null)
    {
        if ((is_a($data, 'Illuminate\Database\Eloquent\Collection') && $data->isEmpty() || !isset($data))) {
            return response()->json(
                ['errors' =>
                     ['code' => 404, 'message' => (isset($id)) ? 'Resource with id :' . $id . ' not found.' : 'Resource not found.']
                ], 404);
        }
        return response()->json(
            [
                'status'  => 'ok',
                'message' => ($msg) ? $msg : 'success',
                'data'    => is_array($data) ? $data: $data->toArray(),

            ], 200);
    }

    /**
     * @param $errors
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function jsonErrorResponse($errors)
    {
        $return = ['errors'];
        foreach ($errors as $error) {
            $return[] = ['code' => 400, 'message' => $error];
        }
        return response()->json(['errors' => $return], 400);
    }

    /**
     * @param \Exception $ex
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function jsonExceptionResponse(\Exception $ex)
    {
        return response()->json(['errors' => ['code' => 500, 'message' => $ex->getMessage()]], 500);
    }

}