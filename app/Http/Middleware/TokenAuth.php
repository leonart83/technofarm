<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class TokenAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->has('api_token')) {


            if (Auth::guard($guard)->user()->toArray() ['api_token'] == $request->input('api_token')) {
                return $next($request);
            }
        }

        if ($request->ajax() || $request->wantsJson()) {
            return response('Unauthorized.', 401);
        } else {
            return redirect()->guest('login');
        }


    }
}
