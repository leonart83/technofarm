<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/**
 *    HTTP VERB    Function    UR
 * 1    GET    This lists all accommodations    /accommodations
 * 2    GET    This shows (reads) a single accommodation    /accommodations/{id}
 * 3    POST    This creates a new accommodation    /accommodations
 * 4    PUT    This entirely modifies (updates) an accommodation    /accommodations/{id}
 * 5    PATCH    This partially modifies (updates) an accommodation    /accommodations/{id}
 * 6    DELETE    This deletes an accommodation    /accommodations/{id}
 */


Route::get('/',     function () { return view('home'); });
Route::get('/home', function () { return view('home'); });


Route::auth();

Route::group([ 'middleware' => [ 'auth' ] ], function () {
    Route::get('/contracts/ownReport', 'FrontEndController@ownReport');
    Route::get('/contracts/create', 'ContractsController@create');
    Route::get('/contracts/edit/{contracts}', 'ContractsController@edit');
    Route::get('/report', function () {
        return view('report');
    });
    //    Route::resource('api', 'ApiController');
});


/*
    * Route::group(['prefix' => 'api/v1/', 'middleware' => ['auth', 'auth:api', 'token']], function () {
    */

Route::group([ 'prefix' => 'api/v1/', 'middleware' => [ 'auth' ] ], function () {
    Route::resource('contracts', 'ContractsController', [ 'except' => [ 'create', 'edit' ] ]);
    Route::resource('owners', 'OwnerController', [ 'except' => [ 'create', 'edit' ] ]);
    Route::resource('plots', 'PlotController', [ 'except' => [ 'create', 'edit' ] ]);
});

