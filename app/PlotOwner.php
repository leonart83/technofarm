<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlotOwner extends Model
{
    public $timestamps = false;
    protected $table = 'plot_owner';
    protected $fillable = [
        'plot_id',
        'owner_id',
    ];
}
