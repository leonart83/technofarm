<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LandOwner
 *
 * @package App
 *
 * 3.	Land owners – the people from who the client rent some land
 */
class Owner extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'surname',
        'phone',
        'personal_id',
    ];
    protected $guarded = [
        'id',
        'personal_id',
    ];
}
