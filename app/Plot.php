<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plot extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'area',
        'contract_id',
        'unique_id',
    ];
    protected $guarded = [
        'unique_id',
        'contract_id',
    ];
    protected $hidden = [
        'contract_id',
    ];

    public function contract()
    {
        return $this->belongsTo('App\Contract');
    }

    /**
     * @return array
     */
    public static function validationRules()
    {
        return [
            'area'        => 'required|numeric|min:0.1',
            'contract_id' => 'required|numeric',
        ];
    }

    public static function validationMessages()
    {
        return [
            'area.required'        => 'Area is required',
            'area.min'        => 'Area has to be greater than zero',
            'contract_id.required' => 'contract_id is required',
            'area.numeric'         => 'Area must be a number',
            'contract_id.numeric'  => 'contract_id must be a number',
        ];
    }

    public static function validate($input)
    {
        $validator = \Validator::make(
            $input,
            self::validationRules(),
            self::validationMessages());

        if ($validator->fails()) {
            return $validator->errors()->all();
        }
        return true;
    }

    /**
     * @param $data
     *
     * @return \Exception|Contract
     */
    public static function store($data)
    {
        try {
            $contract = Contract::findOrFail($data['contract_id']);
            $data['unique_id'] = str_random(36);
            $contract->plots()->create($data);

        } catch (\Exception $ex) {
            return $ex;
        }
        return $contract->load('plots');

    }

    /**
     * @param $data
     *
     * @return \Exception|Plot
     */
    public static function updateModel($data)
    {
        try {
            $plot = Plot::findOrFail($data['plot_id']);
            $plot->owners()->attach ([$data['owner_id']]);
        }
        catch (\Exception $ex) {
            return $ex;
        }
$r = $plot->load('owners');
        return $r;

    }
    public function owners()
    {
        return $this->belongsToMany('App\Owner', 'plot_owner', 'plot_id', 'owner_id');
    }
}
