<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    public $timestamps = false;
    public static $contract_types = [
        'Lease',
        'Ownership',
    ];

    protected $fillable = [
        'contract_type',
        'start_date',
        'due_date',
        'rent_hectare',
        'price',
    ];

    protected $guarded = [ 'id' ];

    protected $with = [ 'plots.owners' ];

    protected $appends = [ 'area', 'price_total' ];

    public function getAreaAttribute()
    {
        $total = 0;
        if (!$this->plots->isEmpty()) {
            $plots = $this->plots->toArray();
            foreach ($plots as $index => $obj) {
                $total += $obj['area'];
            }
        }
        return $total;
    }

    public function getPriceTotalAttribute()
    {
        $total = 0;

        if (!$this->plots->isEmpty()) {
            $plots = $this->plots->toArray();
            foreach ($plots as $index => $obj) {
                $total += $obj['area'] * $this['attributes']['price'];
            }
        }

        return $total;
    }

    /**
     * @return array
     */
    public static function validationRules()
    {
        return [
            'contract_type' => 'required|string|in:' . implode(',', self::$contract_types),
            'start_date'    => 'required|date',
            'due_date'      => 'required|date',
            'rent_hectare'  => 'required_if:contract_type,==,Lease|numeric',
            'price'         => 'required_if:contract_type,==,Ownership|numeric',
        ];
    }

    /**
     * @return array
     */
    public static function validationMessages()
    {
        return [
            'contract_type.required'   => 'Contract type is required',
            'contract_type.string'     => 'Contract type must be a string',
            'contract_type.in'         => 'Contract type must be Lease or Ownership',
            'start_date.required'      => 'Start date is required',
            'start_date.date'          => 'Start date must be a date',
            'due_date.required'        => 'Due date is required',
            'due_date.date'            => 'Due date  must be a date',
            'rent_hectare.required_if' => 'Rent price is required if contract type is Lease',
            'rent_hectare.numeric'     => 'Rent price must be a number',
            'price.required_if'        => 'Price is required if contract type is Ownership',
            'price.numeric'            => 'Price must be a number',
        ];
    }

    /**
     * @param $input
     *
     * @return array|bool
     */
    public static function validate($input)
    {
        $validator = \Validator::make(
            $input,
            self::validationRules(),
            self::validationMessages());

        if ($validator->fails()) {
            return $validator->errors()->all();
        }

        return true;
    }

    /**
     * @param $data
     *
     * @return \Illuminate\Database\Eloquent\Collection | self | \Exception
     *
     */
    public static function store($data)
    {
        try {
            $contract = self::create($data);
            $contract->save();
        } catch (\Exception $ex) {
            return $ex;
        }

        return $contract->load('plots');

    }

    /**
     * @param $data
     *
     * @return \Illuminate\Database\Eloquent\Collection | self | \Exception
     *
     */
    public static function updateModel($data)
    {
        try {
            $contract = self::findOrFail($data['id']);
            $contract->fill($data);
            $contract->save();
        } catch (\Exception $ex) {
            return $ex;
        }
        $contract =  self::findOrFail($data['id']);
        //@todo if record is different show warning; ex : max size rent price reached data is truncated at > 4294967295
        return $contract->load('plots.owners');

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function plots()
    {
        return $this->hasMany('App\Plot', 'contract_id', 'id');
    }
}